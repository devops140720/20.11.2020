USE [Many]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 11/20/2020 11:46:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 11/20/2020 11:46:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](50) NOT NULL,
	[ID_NUMBER] [varchar](9) NOT NULL,
	[COUNTRY_ID] [bigint] NOT NULL,
	[ADDRESS] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 11/20/2020 11:46:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CUSTOMER_ID] [bigint] NOT NULL,
	[PRODUCT_ID] [bigint] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 11/20/2020 11:46:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](50) NOT NULL,
	[PRICE] [real] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Country] ON 

INSERT [dbo].[Country] ([ID], [NAME]) VALUES (1, N'ISRAEL')
INSERT [dbo].[Country] ([ID], [NAME]) VALUES (2, N'USA')
INSERT [dbo].[Country] ([ID], [NAME]) VALUES (3, N'CHINA')
INSERT [dbo].[Country] ([ID], [NAME]) VALUES (4, N'JAPAN')
INSERT [dbo].[Country] ([ID], [NAME]) VALUES (5, N'FRANCE')
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([ID], [NAME], [ID_NUMBER], [COUNTRY_ID], [ADDRESS]) VALUES (2, N'Danny', N'033582652', 1, N'Hertzel 80')
INSERT [dbo].[Customer] ([ID], [NAME], [ID_NUMBER], [COUNTRY_ID], [ADDRESS]) VALUES (4, N'Pazit', N'035792765', 2, N'California')
INSERT [dbo].[Customer] ([ID], [NAME], [ID_NUMBER], [COUNTRY_ID], [ADDRESS]) VALUES (5, N'Hoshimoto', N'969275447', 4, N'TOKYO')
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([ID], [CUSTOMER_ID], [PRODUCT_ID]) VALUES (1, 2, 1)
INSERT [dbo].[Orders] ([ID], [CUSTOMER_ID], [PRODUCT_ID]) VALUES (2, 2, 1)
INSERT [dbo].[Orders] ([ID], [CUSTOMER_ID], [PRODUCT_ID]) VALUES (3, 5, 1)
INSERT [dbo].[Orders] ([ID], [CUSTOMER_ID], [PRODUCT_ID]) VALUES (4, 4, 3)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ID], [NAME], [PRICE]) VALUES (1, N'lAPTOP LENOVO', 8000)
INSERT [dbo].[Product] ([ID], [NAME], [PRICE]) VALUES (2, N'CAMERA', 12000)
INSERT [dbo].[Product] ([ID], [NAME], [PRICE]) VALUES (3, N'MICROPHONE', 5000)
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Country] FOREIGN KEY([COUNTRY_ID])
REFERENCES [dbo].[Country] ([ID])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Country]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Customer] FOREIGN KEY([CUSTOMER_ID])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Customer]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Products] FOREIGN KEY([PRODUCT_ID])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Products]
GO
