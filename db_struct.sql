create table Country
(
    ID   bigint identity
        constraint PK_Country
            primary key,
    NAME varchar(50) not null
)

create table Customer
(
    ID         bigint identity
        constraint PK_Customer
            primary key,
    NAME       varchar(50) not null,
    ID_NUMBER  varchar(9)  not null,
    COUNTRY_ID bigint      not null
        constraint FK_Customer_Country
            references Country,
    ADDRESS    varchar(50) not null
)

create table Product
(
    ID    bigint identity
        constraint PK_Product
            primary key,
    NAME  varchar(50) not null,
    PRICE real        not null
)


create table Orders
(
    ID          bigint identity
        constraint PK_Orders
            primary key,
    CUSTOMER_ID bigint not null
        constraint FK_Orders_Customer
            references Customer,
    PRODUCT_ID  bigint not null
        constraint FK_Orders_Products
            references Product
)

